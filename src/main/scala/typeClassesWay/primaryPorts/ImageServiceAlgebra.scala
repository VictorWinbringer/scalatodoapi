package typeClassesWay.primaryPorts

import domain.images.entities.Image

trait ImageServiceAlgebra[F[_], TImageServiceData] {
  def get(service: TImageServiceData)(id: Long): F[Option[Image]]

  def add(service: TImageServiceData)(b: Image): F[Long]
}

object ImageServiceAlgebra {
  def apply[F[_], TImageServiceData](implicit t: ImageServiceAlgebra[F, TImageServiceData]): ImageServiceAlgebra[F, TImageServiceData] = t

  implicit class ImageServiceOps[F[_], TData](data: TData)(implicit t: ImageServiceAlgebra[F, TData]) {
    def get(id: Long) = t.get(data)(id)

    def add(image: Image) = t.add(data)(image)
  }
}

