package typeClassesWay.secondaryAdapter

import cats.effect.Bracket
import domain.images.entities.Image
import doobie.implicits._
import typeClassesWay.secondaryPorts.ImagesRepoAlgebra

class ImagesRepoTC[F[_]](implicit val bracket: Bracket[F, Throwable]) extends ImagesRepoAlgebra[F, ImagesRepoData[F]] {

  def get(repo: ImagesRepoData[F])(id: Long): F[Option[Image]] = sql"""
         SELECT id, hash, file_path
         FROM images
         WHERE id = ${id}
         """.query[Image]
    .option
    .transact(repo.xa)

  def add(repo: ImagesRepoData[F])(b: Image): F[Long] = sql"""
         INSERT INTO images (hash, file_path)
         VALUES (${b.hash}, ${b.filePath})""".update
    .withUniqueGeneratedKeys[Long]("id")
    .transact(repo.xa)
}
