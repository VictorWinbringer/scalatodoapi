package typeClassesWay.secondaryAdapter

import doobie.util.transactor.Transactor

case class ImagesRepoData[F[_]](xa: Transactor[F])
