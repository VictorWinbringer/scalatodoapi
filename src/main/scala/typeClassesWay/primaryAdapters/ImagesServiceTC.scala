package typeClassesWay.primaryAdapters

import cats.effect.Bracket
import domain.images.entities.Image
import typeClassesWay.primaryPorts.ImageServiceAlgebra
import typeClassesWay.secondaryPorts.ImagesRepoAlgebra._
import typeClassesWay.secondaryPorts.ImagesRepoAlgebra

class ImagesServiceTC[F[_], A](
                                implicit val bracket: Bracket[F, Throwable],
                                implicit val imageRepo: ImagesRepoAlgebra[F, A]
                              ) extends ImageServiceAlgebra[F, ImageServiceData[A]] {

  def get(service: ImageServiceData[A])(id: Long): F[Option[Image]] = service.repo.get(id)

  def add(service: ImageServiceData[A])(b: Image): F[Long] = service.repo.add(b)
}