package typeClassesWay.secondaryPorts

import domain.images.entities.Image


trait ImagesRepoAlgebra[F[_], TRepoData] {
  def get(repo: TRepoData)(id: Long): F[Option[Image]]

  def add(repo: TRepoData)(b: Image): F[Long]
}

object ImagesRepoAlgebra {
  def apply[F[_], TRepoData](implicit t: ImagesRepoAlgebra[F, TRepoData]): ImagesRepoAlgebra[F, TRepoData] = t

  implicit class ImageServiceOps[F[_], TData](data: TData)(implicit t: ImagesRepoAlgebra[F, TData]) {
    def get(id: Long) = t.get(data)(id)

    def add(image: Image) = t.add(data)(image)
  }
}

