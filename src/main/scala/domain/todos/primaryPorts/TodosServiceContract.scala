package domain.todos.primaryPorts

import domain.todos.entities.Todo

trait TodosServiceContract[F[_]] {

  def getAll(): F[List[Todo]]

  def get(id: Long): F[Option[Todo]]

  def add(todo: TodoCreatModel): F[Long]

  def update(todo: Todo): F[Int]

  def delete(id: Long): F[Int]
}


