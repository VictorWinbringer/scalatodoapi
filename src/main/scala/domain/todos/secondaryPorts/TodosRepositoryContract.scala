package domain.todos.secondaryPorts

import domain.common.UnitOfWorkContract
import domain.todos.entities.Todo

trait TodosRepositoryContract[F[_], S[_]] {
  def get(id: Long): UnitOfWorkContract[F, Option[Todo], S]

  def getAll(): UnitOfWorkContract[F, List[Todo], S]

  def add(todo: Todo): UnitOfWorkContract[F, Long, S]

  def update(todo: Todo): UnitOfWorkContract[F, Int, S]

  def remove(id: Long): UnitOfWorkContract[F, Int, S]
}
