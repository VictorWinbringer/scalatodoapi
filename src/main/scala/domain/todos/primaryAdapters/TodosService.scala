package domain.todos.primaryAdapters

import domain.common.InstantsRepositoryContract
import domain.todos.entities.Todo
import domain.todos.primaryPorts.{TodoCreatModel, TodosServiceContract}
import domain.todos.secondaryPorts.TodosRepositoryContract
import cats.Monad
import cats.implicits._

class TodosService[F[_] : Monad, S[_]](
                                        implicit val repo: TodosRepositoryContract[F, S],
                                        implicit val instants: InstantsRepositoryContract[F]
                                      ) extends TodosServiceContract[F] {

  def getAll(): F[List[Todo]] =
    repo.getAll().commit()

  def add(todo: TodoCreatModel): F[Long] = for {
    now <- instants.createNow()
    id <- repo.add(Todo(id = 0, name = todo.name, imageId = todo.imageId, created = now)).commit()
  } yield id

  def get(id: Long): F[Option[Todo]] =
    repo.get(id).commit()

  def update(todo: Todo): F[Int] =
    repo.update(todo).commit()

  def delete(id: Long): F[Int] =
    repo.remove(id).commit()
}
