package domain.todos.entities

import java.time.Instant

case class Todo(id: Long, name: String, imageId: Long, created: Instant)
