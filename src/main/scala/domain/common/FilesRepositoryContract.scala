package domain.common

import java.nio.file.Path

trait FilesRepositoryContract[F[_], S[_]] {

  def upload(stream: S[Byte], path: Path): F[Unit]

  def download(path: Path): S[Byte]

  def size(path: Path): F[Long]

  def exists(path: Path): F[Boolean]
}










