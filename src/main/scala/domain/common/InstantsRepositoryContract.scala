package domain.common

import java.time.Instant

trait InstantsRepositoryContract[F[_]]{
  def createNow():F[Instant]
}
