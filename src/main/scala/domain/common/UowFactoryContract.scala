package domain.common

trait UowFactoryContract[F[_], S[_]] {
  def create(): UnitOfWorkContract[F, Unit, S]
}
