package domain.common

trait Hashable[F[_], A] {
  def hash(a: A): F[String]
}

object Hashable {

  implicit class HashableOps[F[_], A](a: A)(implicit t: Hashable[F, A]) {
    def hash() = t.hash(a)
  }

}