package domain.common

trait Splittable[F[_], A] {
  def split(a: A): F[(A, A)]
}

object Splittable {

  implicit class SplittableOps[F[_], A](a: A)(implicit t: Splittable[F, A]) {
    def split() = t.split(a)
  }

}