package domain.common

trait UnitOfWorkContract[F[_], A, State[_]] {

  def state(): State[A]

  def commit(): F[A]

  def map[B](f: A => B): UnitOfWorkContract[F, B, State]

  def flatMap[B](f: A => UnitOfWorkContract[F, B, State]): UnitOfWorkContract[F, B, State]
}

