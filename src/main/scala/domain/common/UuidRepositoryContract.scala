package domain.common

import java.util.UUID

trait UuidRepositoryContract[F[_]] {
  def create(): F[UUID]
}
