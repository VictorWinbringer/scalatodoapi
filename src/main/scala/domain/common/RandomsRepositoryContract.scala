package domain.common

trait RandomsRepositoryContract[F[_]] {

  def nexInt(): F[Int]
}
