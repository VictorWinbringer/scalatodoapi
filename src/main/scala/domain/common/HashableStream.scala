package domain.common

import cats.Monad
import cats.implicits._
import fs2.Stream

class HashableStream[F[_] : Monad](implicit compiler: fs2.Stream.Compiler[F, F]) extends Hashable[F, Stream[F, Byte]] {
  def hash(a: Stream[F, Byte]): F[String] = a.through(fs2.hash.md5).map(_.toHexString).compile.toVector.map(l => l.mkString(""))
}
