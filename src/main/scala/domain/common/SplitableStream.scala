package domain.common

import cats.effect.{Concurrent, ContextShift}
import cats.implicits._
import cats.{Applicative, Functor}
import fs2.Stream
import fs2.concurrent.Queue

class SplittableStream[F[_] : Concurrent : Applicative : Functor, A](implicit c: ContextShift[F]) extends Splittable[F, Stream[F, A]] {
  def split(a: Stream[F, A]): F[(Stream[F, A], Stream[F, A])] = {
    for {
      q <- Queue.noneTerminated[F, A]
    } yield (a.evalTap(b => q.enqueue1(Some(b))).onFinalize(q.enqueue1(None)), q.dequeue)
  }
}
