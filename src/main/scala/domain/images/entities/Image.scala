package domain.images.entities

case class Image(id: Long, hash: String, filePath: String)
