package domain.images.primaryPorts

import domain.images.entities.Image
import fs2.Stream

trait ImagesServiceContract[F[_], S[_]] {

  def upload(stream: S[Byte]): F[Long]

  def download(id: Long): F[Option[(Long, S[Byte])]]

  def getAll(): F[List[Image]]
}
