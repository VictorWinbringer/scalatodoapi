package domain.images.secondaryPorts

import domain.common.UnitOfWorkContract
import domain.images.entities.Image

trait ImagesRepositoryContract[F[_], S[_]] {

  def get(id: Long): UnitOfWorkContract[F, Option[Image], S]

  def getAll(): UnitOfWorkContract[F, List[Image], S]

  def add(image: Image): UnitOfWorkContract[F, Long, S]
}




