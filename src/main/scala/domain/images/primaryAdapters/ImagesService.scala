package domain.images.primaryAdapters

import java.nio.file.Paths

import cats.data.OptionT
import cats.implicits._
import cats.{Applicative, Monad}
import domain.common.Hashable._
import domain.common.Splittable._
import domain.common.{FilesRepositoryContract, Hashable, Splittable, UuidRepositoryContract}
import domain.images.entities.Image
import domain.images.primaryPorts.ImagesServiceContract
import domain.images.secondaryPorts.ImagesRepositoryContract

class ImagesService[F[_] : Monad, S[_], FS[_] : Monad](
                                                        implicit val fr: FilesRepositoryContract[F, FS],
                                                        implicit val ir: ImagesRepositoryContract[F, S],
                                                        implicit val ur: UuidRepositoryContract[F],
                                                        implicit val hashable: Hashable[F, FS[Byte]],
                                                        implicit val splittable: Splittable[F, FS[Byte]],
                                                        implicit val fsync: Applicative[F]
                                                      )
  extends ImagesServiceContract[F, FS] {

  def upload(stream: FS[Byte]): F[Long] =
    for {
      (h, p) <- create(stream)
      imageId <- ir.add(Image(0, h, p)).commit()
    } yield imageId

  private def create(stream: FS[Byte]): F[(String, String)] = for {
    (s1, s2) <- stream.split()
    hash <- s1.hash()
    path = Paths.get(s"images/${hash}")
    exists <- fr.exists(path)
    res = if (exists) {
      fsync.pure(())
    } else {
      fr.upload(s2, path)
    }
    _ <- res
  } yield (hash, path.toString)

  def download(id: Long): F[Option[(Long, FS[Byte])]] = (for {
    image <- OptionT(ir.get(id).commit())
    stream <- OptionT.liftF(getImageStream(image))
  } yield stream).value

  def getImageStream(image: Image): F[(Long, FS[Byte])] = {
    val paths = Paths.get(image.filePath)
    for {
      size <- fr.size(paths)
    } yield (size, fr.download(paths))
  }

  def getAll(): F[List[Image]] =
    ir.getAll().commit()
}
