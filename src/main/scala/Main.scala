import appServices.{ChatHub, ClientExamples, ImagesController, TestController, TodosController}
import cats.effect.{IO, _}
import cats.implicits._
import com.typesafe.scalalogging.StrictLogging
import domain.common.{HashableStream, SplittableStream}
import domain.images.primaryAdapters.ImagesService
import domain.todos.primaryAdapters.TodosService
import doobie._
import doobie.util.ExecutionContexts
import infrastructure.Types.StreamIO
import infrastructure._
import infrastructure.settings.AppSettings
import org.flywaydb.core.Flyway
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.{Router, Server => H4Server}
import org.http4s.syntax.kleisli._
import pureconfig._
import sttp.tapir.docs.openapi._
import sttp.tapir.openapi.circe.yaml._
import sttp.tapir.server.http4s._
import sttp.tapir.swagger.http4s.SwaggerHttp4s
import pureconfig.generic.auto._
import org.log4s._

object Main extends IOApp {
  private[this] val logger = getLogger

  def create: Resource[IO, H4Server[IO]] = for {
    settings <- Resource.liftF(IO {
      val config = ConfigSource.default.load[AppSettings]
        .map(baseSettings => {
          logger.warn(baseSettings.toString)
          baseSettings
        })
      config match {
        case Right(s) => s
        case Left(e) => throw new Exception(e.toString)
      }
    })
    serverEc <- ExecutionContexts.cachedThreadPool[IO]
    transactorEc <- ExecutionContexts.cachedThreadPool[IO]
    blocker <- Blocker[IO]
    xa = Transactor.fromDriverManager[IO](
      "org.postgresql.Driver",
      settings.db.url,
      settings.db.user,
      settings.db.password,
      Blocker.liftExecutionContext(transactorEc)
    )
    //Пример работы через Type Class
    //    _ <- Resource.liftF(IO {
    //      import typeClassesWay.primaryPorts.ImageServiceAlgebra._
    //      val repo = ImagesRepoData[IO](xa = xa)
    //      val service = ImageServiceData[ImagesRepoData[IO]](repo = repo)
    //      implicit val imagesRepo: ImagesRepoAlgebra[IO, ImagesRepoData[IO]] = new ImagesRepoTC()
    //      implicit val imagesService: ImageServiceAlgebra[IO, ImageServiceData[ImagesRepoData[IO]]] = new ImagesServiceTC()
    //      for {
    //        i <- service.get(1)
    //        b <- service.add(Image(1, "", ""))
    //      } yield ()
    //    })
    _ <- Resource.liftF(IO {
      val flyway = Flyway
        .configure()
        .dataSource(settings.db.url, settings.db.user, settings.db.password)
        .load()
      flyway.migrate()
    })
    implicit0(fr: FilesRepository) = new FilesRepository(blocker)
    implicit0(ir: ImagesRepository) = new ImagesRepository(xa)
    implicit0(tr: TodosRepository) = new TodosRepository(xa)
    implicit0(ur: UuidRepository[IO]) = new UuidRepository[IO]()
    implicit0(dr: InstantsRepository) = new InstantsRepository()
    implicit0(hstr: HashableStream[IO]) = new HashableStream[IO]()
    implicit0(sstr: SplittableStream[IO, Byte]) = new SplittableStream[IO, Byte]()
    implicit0(ts: TodosService[IO, doobie.ConnectionIO]) = new TodosService[IO, doobie.ConnectionIO]()
    implicit0(is: ImagesService[IO, doobie.ConnectionIO, StreamIO]) = new ImagesService[IO, doobie.ConnectionIO, StreamIO]()
    todosController = new TodosController()
    imagesController = new ImagesController()
    testController = new TestController()
    chatHub <- Resource.liftF(ChatHub[IO]())
    endpoints = todosController.endpoints ::: imagesController.endpoints ::: testController.endpoints
    docs = (chatHub.endpointWs :: endpoints).toOpenAPI("The Scala Todo List", "0.0.1")
    yml: String = docs.toYaml
    routes = chatHub.routeWs <+>
      endpoints.toRoutes <+>
      new SwaggerHttp4s(yml, "swagger").routes[IO]
    httpApp = Router(
      "/" -> routes
    ).orNotFound
    //Пример работы с транзакцией
    //    res = for {
    //      _ <- new UowFactory(xa).create()
    //      id <- tr.add(Todo(1, "ddd", 1, Instant.now()))
    //      a <- tr.get(id)
    //      b = a.get.copy(name = "Updateed")
    //      c <- tr.update(b)
    //      d <- tr.get(id)
    //    } yield (id, a, b, c, d)
    //    rrr <- Resource.liftF(res.commit())
    //    _ <- Resource.liftF(IO {
    //      println(rrr)
    //    })
    // _ <- Resource.liftF(ClientExamples.execute())
    blazeServer <- BlazeServerBuilder[IO](serverEc)
      .bindHttp(settings.host.port, settings.host.host)
      .withHttpApp(httpApp)
      .resource
  } yield blazeServer

  override def run(args: List[String]): IO[ExitCode] =
    create.use { _ => IO.never }.as(ExitCode.Success)
}