package appServices

import cats.effect.{ContextShift, IO}
import domain.todos.entities.Todo
import io.circe.generic.auto._
import sttp.model.CookieWithMeta
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.{header, _}

class TestController(implicit contextShift: ContextShift[IO]) extends ControllerBase {

  private val baseTestEndpoint = baseEndpoint
    .in("test")
    .tag("Test")

  private val postTest = baseTestEndpoint
    .summary("Тестовый эндпойнт для запроска к самому себе")
    .description("Возвращает тестовые данные")
    .post
    .in(header[String]("test_header"))
    .in(jsonBody[List[Todo]])
    .in(cookies)
    .out(header[String]("test_header_out"))
    .out(jsonBody[List[Todo]])
    .out(setCookies)
    .serverLogic(x => withStatus(IO {
      (x._1 + x._3.map(c => c.name + "" + c.value).fold("")((a, b) => a + " " + b), x._2, List(CookieWithMeta(name = "test", value = "test_value")))
    }))

  private val runHttpRequestTes = baseTestEndpoint
    .summary("Запускает тестовый запрос к самому себе")
    .description("Запускает тестовый запрос к самому себе")
    .get
    .out(stringBody)
    .serverLogic(_ => withStatus(runHttp()))

  def runHttp(): IO[String] = {
    ClientExamples.execute().as("Ok")
  }

  val endpoints = List(
    postTest,
    runHttpRequestTes
  )
}
