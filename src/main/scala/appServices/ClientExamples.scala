package appServices

import cats.effect.{Blocker, ContextShift, IO}
import com.typesafe.scalalogging.StrictLogging
import domain.todos.entities.Todo
import io.circe.generic.auto._
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.client.blaze._
import org.http4s.client.middleware.Logger
import org.http4s.headers._
import org.http4s.multipart.{Multipart, Part}
import org.http4s.{MediaType, Uri, _}
import org.log4s._
import sttp.tapir.RawBodyType

import java.time.Instant
import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.global

object ClientExamples extends StrictLogging {
  private[this] val logger = getLogger

  def execute()(implicit contextShift: ContextShift[IO]) = {
    BlazeClientBuilder[IO](global).resource.use { client =>
      logger.warn("Start Request")
      val loggedClient = Logger[IO](true, true)(client)
      val uri = Uri.fromString("http://localhost:8080/api/v1/test").toOption.get
      val request: Request[IO] = Request[IO](method = Method.POST, uri = uri)
        .withEntity(List(Todo(1, "Test", 2, Instant.now())))
        .withHeaders(Accept(MediaType.application.json), Header(name = "test_header", value = "test_header_value"))
        .addCookie("test_cookie", "test_cookie_value")
      loggedClient.run(request).use(r => {
        logger.warn("End Request")
        logger.warn(r.status.toString())
        logger.warn(r.toString())
        logger.warn(r.headers.toString())
        r.bodyText.map(t => logger.warn(t)).compile.drain
      })
    }
  }
}
