package appServices

import java.io._

import cats.effect.IO
import domain.images.entities.Image
import domain.images.primaryPorts.ImagesServiceContract
import io.circe.generic.auto._
import fs2._
import infrastructure.Types.StreamIO
import sttp.model.HeaderNames
import sttp.tapir._
import sttp.tapir.json.circe._

class ImagesController(implicit val imagesService: ImagesServiceContract[IO, StreamIO]) extends ControllerBase {

  private val baseImageEndpoint = baseEndpoint
    .in("images")
    .tag("Images")

  private val upload = baseImageEndpoint
    .summary("Загрузить файл картинки на сервер")
    .description("Загружает выбранную картинку")
    .post
    .in(streamBody[Stream[IO, Byte]](schemaFor[File], CodecFormat.OctetStream()))
    .out(jsonBody[Long])
    .serverLogic(x => withStatus(imagesService.upload(x)))

  private val download = baseImageEndpoint
    .summary("Скачать картинку")
    .description("Скачивает картику по ее идентификатору")
    .get
    .in(path[Long]("id"))
    .out(header[Long](HeaderNames.ContentLength))
    .out(streamBody[Stream[IO, Byte]](schemaFor[File], CodecFormat.OctetStream()))
    .serverLogic(x => withStatus(imagesService.download(x).map(x => x.get)))

  private val getAll = baseImageEndpoint
    .summary("Получить список картинок")
    .description("Получает список всех картинок с сервера")
    .get
    .out(jsonBody[List[Image]])
    .serverLogic(_ => withStatus(imagesService.getAll()))

  val endpoints = List(
    upload,
    download,
    getAll
  )
}
