package appServices

import cats.effect.IO
import domain.todos.entities.Todo
import domain.todos.primaryPorts.{TodoCreatModel, TodosServiceContract}
import io.circe.generic.auto._
import sttp.model.HeaderNames
import sttp.tapir.EndpointInput.Cookie
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.{header, path, _}




class TodosController(implicit val todoService: TodosServiceContract[IO]) extends ControllerBase {

  private val baseTodoEndpoint = baseEndpoint
    .in("todos")
    .tag("Todos")

  private val getAll = baseTodoEndpoint
    .summary("Показать все задачи")
    .description("Показывает список задач")
    .get
    .out(jsonBody[List[Todo]])
    .serverLogic(_ => withStatus(todoService.getAll()))

  private val getById = baseTodoEndpoint
    .summary("Получить задачу по индентификатору")
    .description("Показывает задачу по ее идентификатору")
    .get
    .in(path[Long]("id"))
    .out(jsonBody[Option[Todo]])
    .serverLogic(x => withStatus(todoService.get(x)))

  private val add = baseTodoEndpoint
    .summary("Создать задачу")
    .description("Создает задачу")
    .post
    .in(jsonBody[TodoCreatModel])
    .out(jsonBody[Long])
    .serverLogic(x => withStatus(todoService.add(x)))


  private val update = baseTodoEndpoint
    .summary("Обновяет задачу")
    .description("Обновить задачу")
    .put
    .in(jsonBody[Todo])
    .out(jsonBody[Int])
    .serverLogic(x => withStatus(todoService.update(x)))

  private val remove = baseTodoEndpoint
    .summary("Удалить задачу")
    .description("Удаляет задачу")
    .delete
    .in(path[Long]("id"))
    .out(jsonBody[Int])
    .serverLogic(x => withStatus(todoService.delete(x)))

  val endpoints = List(
    getAll,
    getById,
    add,
    update,
    remove
  )
}
