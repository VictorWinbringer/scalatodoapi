package infrastructure

import java.nio.file.{Path, StandardOpenOption}

import cats.effect.{Blocker, ContextShift, IO, Sync}
import domain.common.FilesRepositoryContract
import fs2.{Stream, io}
import infrastructure.Types.StreamIO

class FilesRepository(val blocker: Blocker)(implicit val contextShift: ContextShift[IO], implicit val sf: Sync[IO]) extends FilesRepositoryContract[IO, StreamIO] {

  def upload(stream: StreamIO[Byte], path: Path): IO[Unit] =
    stream
      .through(fs2.io.file.writeAll[IO](path, blocker, List(StandardOpenOption.CREATE, StandardOpenOption.WRITE)))
      .compile
      .drain

  def download(path: Path): Stream[IO, Byte] =
    io.file.readAll[IO](path, blocker, 4096)

  def size(path: Path): IO[Long] =
    io.file.size[IO](blocker, path)

  def exists(path: Path): IO[Boolean] =
    io.file.exists(blocker, path)
}



