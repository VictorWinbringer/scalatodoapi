package infrastructure

import domain.common.UnitOfWorkContract
import infrastructure.Types.{ListIO, StreamIO}

class FilesUnitOfWork[A](streamIO: StreamIO[A]) extends UnitOfWorkContract[ListIO, A, StreamIO] {
  def state(): StreamIO[A] = streamIO

  def commit(): ListIO[A] = streamIO.compile.toList

  def map[B](f: A => B): FilesUnitOfWork[B] = new FilesUnitOfWork(streamIO.map(f))

  def flatMap[B](f: A => UnitOfWorkContract[ListIO, B, StreamIO]): UnitOfWorkContract[ListIO, B, StreamIO] = new FilesUnitOfWork[B](streamIO.flatMap(x => f(x).state()))
}
