package infrastructure.settings

case class HostSettings(port: Int, host: String)
