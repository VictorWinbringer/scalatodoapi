package infrastructure.settings

case class AppSettings(db: DbSettings, host: HostSettings, environment: String)


