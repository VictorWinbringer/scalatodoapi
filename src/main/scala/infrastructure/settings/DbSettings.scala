package infrastructure.settings

case class DbSettings(url: String, user: String, password: String)
