package infrastructure

import cats.effect.IO
import domain.images.entities.Image
import domain.images.secondaryPorts.ImagesRepositoryContract
import doobie.implicits._
import doobie.util.query.Query0
import doobie.util.transactor.Transactor
import doobie.util.update.Update0

class ImagesRepository(val xa: Transactor[IO]) extends ImagesRepositoryContract[IO, doobie.ConnectionIO] {

  import ImagesRepositorySql._

  def get(id: Long): UnitOfWork[Option[Image]] = new UnitOfWork[Option[Image]](xa, select(id)
    .option)

  def getAll(): UnitOfWork[List[Image]] = new UnitOfWork[List[Image]](xa, select().to[List])

  def add(image: Image): UnitOfWork[Long] = new UnitOfWork[Long](xa, insert(image)
    .withUniqueGeneratedKeys[Long]("id"))
}

object ImagesRepositorySql {
  def select(id: Long): Query0[Image] = sql"""
         SELECT id, hash, file_path
         FROM images
         WHERE id = ${id}
         """.query[Image]

  def select(): Query0[Image] = sql"""
         SELECT id, hash, file_path
         FROM images
         """.query[Image]

  def insert(image: Image): Update0 =
    sql"""
         INSERT INTO images (hash, file_path)
         VALUES (${image.hash}, ${image.filePath})""".update
}
