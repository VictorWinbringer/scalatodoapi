package infrastructure

import java.util.UUID

import cats.effect.Sync
import domain.common.UuidRepositoryContract

class UuidRepository[F[_] : Sync](implicit F: Sync[F]) extends UuidRepositoryContract[F] {
  def create(): F[UUID] = F.delay(UUID.randomUUID())
}
