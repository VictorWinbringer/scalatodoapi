package infrastructure

import java.time.Instant

import cats.effect.IO
import domain.common.{InstantsRepositoryContract, RandomsRepositoryContract}

import scala.util.Random

class RandomsRepository(val random: Random) extends RandomsRepositoryContract[IO] {
  def nexInt(): IO[Int] = IO.delay(random.nextInt())
}


