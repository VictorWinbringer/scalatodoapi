package infrastructure

import cats.effect.IO
import domain.common.UnitOfWorkContract
import doobie._
import doobie.implicits._
import doobie.util.transactor.Transactor

class UnitOfWork[A](val xa: Transactor[IO], val connectionIO: ConnectionIO[A]) extends UnitOfWorkContract[IO, A, ConnectionIO] {

  def commit(): IO[A] = connectionIO.transact(xa)

  def map[B](f: A => B): UnitOfWork[B] = new UnitOfWork[B](xa, connectionIO.map(f))

  def flatMap[B](f: A => UnitOfWorkContract[IO, B, ConnectionIO]): UnitOfWork[B] = new UnitOfWork[B](xa, connectionIO.flatMap(x => f(x).state()))

  def state(): doobie.ConnectionIO[A] = connectionIO
}


