package infrastructure

import cats.effect.IO
import domain.common.{UnitOfWorkContract, UowFactoryContract}
import doobie.`enum`.TransactionIsolation
import doobie.hi.connection
import doobie.util.transactor.Transactor

class UowFactory(val xa: Transactor[IO]) extends UowFactoryContract[IO,doobie.ConnectionIO] {
  def create(): UnitOfWorkContract[IO, Unit,doobie.ConnectionIO] = new UnitOfWork[Unit](xa, connection.setTransactionIsolation(TransactionIsolation.TransactionReadCommitted))
}
