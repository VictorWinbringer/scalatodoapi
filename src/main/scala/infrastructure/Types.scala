package infrastructure

import cats.effect.IO
import fs2.Stream

object Types {
  type ListIO[A] = IO[List[A]]
  type StreamIO[A] = Stream[IO, A]
}
