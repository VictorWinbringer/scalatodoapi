package infrastructure

import java.time.Instant

import cats.effect.IO
import domain.common.InstantsRepositoryContract

class InstantsRepository extends  InstantsRepositoryContract[IO]{
  def createNow(): IO[Instant] = IO.delay(Instant.now())
}
