package infrastructure

import java.time.Instant

import cats.Monad
import cats.effect.{IO}
import domain.todos.entities.Todo
import domain.todos.secondaryPorts.TodosRepositoryContract
import doobie.util.transactor.Transactor
import doobie.implicits._
import cats._
import cats.implicits._
import doobie._
import doobie.implicits.legacy.instant._

class TodosRepository(val xa: Transactor[IO]) extends TodosRepositoryContract[IO, doobie.ConnectionIO] {

  def get(id: Long): UnitOfWork[Option[Todo]] = new UnitOfWork[Option[Todo]](xa, sql"""
         SELECT id, name, image_id, created
         FROM todos
         WHERE id = ${id}
         """.query[Todo]
    .option)

  def getAll(): UnitOfWork[List[Todo]] = new UnitOfWork[List[Todo]](xa, sql"""
         SELECT id, name, image_id, created
         FROM todos
         """.query[Todo]
    .to[List])

  def add(todo: Todo): UnitOfWork[Long] = new UnitOfWork[Long](xa, sql"""
         INSERT INTO todos (name, image_id, created)
         VALUES (${todo.name}, ${todo.imageId}, ${todo.created})
         """.update
    .withUniqueGeneratedKeys[Long]("id"))

  def update(todo: Todo): UnitOfWork[Int] = new UnitOfWork[Int](xa,
    sql"""
         UPDATE todos
         SET name = ${todo.name}, image_id = ${todo.imageId}, created = ${todo.created}
         WHERE id = ${todo.id}
         """.update
      .run)

  def remove(id: Long): UnitOfWork[Int] = new UnitOfWork[Int](xa,
    sql"""
         DELETE FROM todos WHERE id = $id
         """.update
      .run)
}
