package domainTests

import domain.common.FilledStr
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks


class FilledStrTests extends AnyFlatSpec with should.Matchers with ScalaCheckPropertyChecks {

  "equals" should "return true fro equal value" in {
    val str = "1234AB"
    val a = FilledStr(str).get
    val b = FilledStr(str).get
    b.equals(a) should be(true)
  }

  it should "return false for non equal value" in {
    val a = FilledStr("1234AB").get
    val b = FilledStr("BA4321").get
    b.equals(a) should be(false)
  }

  "hashCode" should "return equal value for equal str" in {
    val a = FilledStr("1234AB").get
    val b = FilledStr("1234AB").get
    a.hashCode() should be(b.hashCode())
  }

  it should "return different value for different str" in {
    val a = FilledStr("1234AB").get
    val b = FilledStr("BA4321").get
    a.hashCode() should not be (b.hashCode())
  }

  "constructor" should "save expected value" in {
    forAll { s: String =>
      whenever(s.trim.nonEmpty) {
        val a = FilledStr(s).get
        a.value should be(s)
      }
    }
  }
}
