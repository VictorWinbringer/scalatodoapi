package domainTests

import java.time.Instant

import cats.Id
import domain.common.{InstantsRepositoryContract, UnitOfWorkContract}
import domain.todos.entities.Todo
import domain.todos.primaryAdapters.TodosService
import domain.todos.secondaryPorts.TodosRepositoryContract
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class TodoServiceTests extends AnyFlatSpec with MockFactory with should.Matchers {
  "geAll" should "возвращать ожидаемые значения" in {
    implicit val tr = mock[TodosRepositoryContract[Id, Id]]
    implicit val ir = mock[InstantsRepositoryContract[Id]]
    implicit val uow = mock[UnitOfWorkContract[Id, List[Todo], Id]]
    val m = new TodosService[Id, Id]()
    val list: Id[List[Todo]] = List(Todo(1, "2", 3, Instant.now()))
    (tr.getAll _).expects().returning(uow).once()
    (uow.commit _).expects().returning(list).once()
    m.getAll() should be(list)
  }
}
